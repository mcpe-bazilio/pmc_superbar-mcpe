<?PHP
namespace FaigerSYS\superBAR;
use pocketmine\scheduler\PluginTask;
use pocketmine\Server;
use pocketmine\plugin\Plugin;

class hotBAR extends PluginTask {
	/* @var Server $serv*/
	public $serv;

	public $eT;
	public $noF;
	public $ppup;
	public $CASH;
	public $FACT;

	/* @var Plugin|Main $PP*/
	public $PP;
	public $PP_v;
	public $KD;
	public $FRMT;
	public $PFM;
	public $TIME_FRMT;
	
	public function ddts($tz) {
		if ($tz)
			date_default_timezone_set($tz);
	}
	
	public function onRun($tick) {

		$load = $this->serv->getTickUsage();
		$tps = $this->serv->getTicksPerSecond();
		$plon = count($this->serv->getOnlinePlayers());
		$mxon = $this->serv->getMaxPlayers();
		$a = 0;
		foreach ($this->serv->getOnlinePlayers() as $p) {
			if ($p != null) {
				$name = $p->getName();
				
				if ($this->PP) {
					if ($this->PP_v == '1.1' || $this->PP_v == '1.0')
						$ppg = $a = $this->PP->getUser($p)->getGroup()->getName();
					else
						$ppg = $a = $this->PP->getUserDataMgr()->getData($p)['group'];
				} else
					$ppg = '§cNoPPplug';
				
				if ($this->FACT) {
					if (count($fact = $this->FACT->getPlayerFaction($name)) == 0)
						$fact = $this->noF[$a];
				} else $fact = '§cNoFactPlug';
				
				if ($this->eT == 1)
					$cash = $this->CASH->myMoney($name);
				elseif ($this->eT == 2)
					$cash = $this->CASH->getMoney($name);
				else
					$cash = '§cNoEcoPlug';
				
				if ($this->KD) {
					$kll = $this->KD->getKills($name);
					$dth = $this->KD->getDeaths($name);
				} else
					$kll = $dth =  '§cNoPlug';
				
				if ($p->getInventory() != null) {
					$id = $p->getInventory()->getItemInHand()->getId();
					$mt = $p->getInventory()->getItemInHand()->getDamage();
				} else
					$id = $mt = 0;

				$r = ($p->yaw - 90) % 360;
				if($r < 0){
					$r += 360.0;
				}
				if((0 <= $r and $r < 23) or (338 <= $r and $r < 360)){
					$direction = 'север';
				}elseif(23 <= $r and $r < 68){
					$direction = 'северо-восток';
				}elseif(68 <= $r and $r < 113){
					$direction = 'восток';
				}elseif(113 <= $r and $r < 158){
					$direction = 'юго-восток';
				}elseif(158 <= $r and $r < 203){
					$direction = 'юг';
				}elseif(203 <= $r and $r < 248){
					$direction = 'юго-запад';
				}elseif(248 <= $r and $r < 293){
					$direction = 'запад';
				}elseif(293 <= $r and $r < 338){
					$direction = 'северо-запад';
				}else{
					$direction = "§c($r градусов).";
				}






				$time = date($this->TIME_FRMT[$a]);
				$text = str_replace(array('%NICK%', '%MONEY%', '%FACTION%', '%ITEM_ID%', '%ITEM_META%', '%TIME%', '%ONLINE%', '%MAX_ONLINE%', '%X%', '%Y%', '%Z%', '%DIRECTION%', '%IP%', '%PP_GROUP%', '%TAG%', '%LOAD%', '%TPS%', '%KILLS%', '%DEATHS%', '%LEVEL%'), array($name, $cash, $fact, $id, $mt, $time, $plon, $mxon, intval($p->x), intval($p->y), intval($p->z), $direction, $p->getAddress(), $ppg, $p->getNameTag(), $load, $tps, $kll, $dth, $p->getLevel()->getName()), $this->FRMT[$a]);
				if ($this->ppup[$a])
					$p->sendPopup($text);
				else
					$p->sendTip($text);
			}
		}
	}
}
